﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;
using ShoppingList.sdk;

namespace ShoppingList.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ShoppingListsController : ControllerBase
    {
        [HttpGet]
        public List<MyShoppingList> GetShoppingLists()
        {
            return ShoppingListRepo._shoppingLists;
        }

        [HttpPost]
        public void Post(MyShoppingList list)
        {
            ShoppingListRepo._shoppingLists.Add(list);
        }

        [HttpDelete]
        public void Delete(Guid guid)
        {
            ShoppingListRepo._shoppingLists.Remove(ShoppingListRepo._shoppingLists.Find(p => p.Guid == guid));
        }

    }

    public static class ShoppingListRepo
    {
        public static List<MyShoppingList> _shoppingLists = new List<MyShoppingList>();
        
        
    }
    [Route("api/ShoppingLists/{listId}/")]
    [ApiController]
    public class ShoppingListController : ControllerBase
    {
        [HttpGet]
        public MyShoppingList GetShoppingLists(Guid listId)
        {
            return ShoppingListRepo._shoppingLists.FirstOrDefault(p => p.Guid == listId);
        }

        [HttpPost]
        public void Post(MyShoppingList list)
        {
           // _shoppingLists.Add(list);
        }

        [HttpDelete]
        public void Delete(Guid guid)
        {
          //  _shoppingLists.Remove(_shoppingLists.Find(p => p.Guid == guid));
        }

    }
    [Route("api/ShoppingLists/{listId}/{itemId}")]
    [ApiController]
    public class ShoppingListItemsController : ControllerBase
    {
        [HttpGet]
        public IActionResult GetShoppingLists(Guid listId,string itemId,string apiKey)
        {
            if (apiKey == "MY-API-KEY")
            {
                return StatusCode(404,
                    ShoppingListRepo._shoppingLists.FirstOrDefault(p => p.Guid == listId).Items
                        .FirstOrDefault(p => p.Name == itemId));
           
            }

            return StatusCode(403);
        }

        [HttpPost]
        public void Post(MyShoppingList list)
        {
            // _shoppingLists.Add(list);
        }

        [HttpDelete]
        public void Delete(Guid guid)
        {
            //  _shoppingLists.Remove(_shoppingLists.Find(p => p.Guid == guid));
        }

    }


}