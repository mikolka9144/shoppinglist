﻿using System;
using System.Collections.Generic;

namespace ShoppingList.sdk
{
    public class MyShoppingList
        {
            public User CreatedBy { get; set; }
            public Guid Guid { get; set; }
            public string Name { get; set; }
            public List<ShoppingItem> Items { get; set; }

        }
    }

