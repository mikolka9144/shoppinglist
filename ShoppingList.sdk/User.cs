﻿using System;

namespace ShoppingList.sdk
{
    public class User
        {
            public string Name { get; set; }
            public Guid Guid { get; set; }
            public User Wife { get; set; }
        }
    }

