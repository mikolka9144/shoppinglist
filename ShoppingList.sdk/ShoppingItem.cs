﻿namespace ShoppingList.sdk
{
    public class ShoppingItem
        {
            public string Name { get; set; }
            public decimal MaxPrice { get; set; }
            public decimal Quantity { get; set; }

        }
    }

